# CiviCRM Vagrant image

This is an attempt at a fairly minimal LEMP Vagrant environment.

- Shell provisioning.
- Stock recent (as of now!) LTS Ubuntu base.

## Installation

- Clone this repository
- Install the plugins `vagrant-env` and `vagrant-hostmanager`

```bash
vagrant plugin install vagrant-env
vagrant plugin install vagrant-hostmanager
```

## Configuration

Configuration assumes you're using the above plugins. Copy `env.example` in the cloned repo to `.env` and edit it to set your custom hostname and other configurable values.

## Usage

Webroot is in `/vagrant/web`. You can put your codebase there or you can use this as a base structure for a Drupal 8 style monorepo.

## Provider

You can swap out Virtualbox for LXC or whatever if you like, just pick a box that supports your provider. IDK why Ubuntu loves Virtualbox and IDC.
