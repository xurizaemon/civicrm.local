#!/bin/bash
echo "Configuring NginX for Vagrant environment."
cp /vagrant/config/nginx/default /etc/nginx/sites-available/
cp /vagrant/config/nginx/drupal /etc/nginx/
service nginx restart
