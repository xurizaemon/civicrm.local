#!/bin/bash
echo "Configuring MySQL for Vagrant environment."
cp /vagrant/config/mysql/local.cnf /etc/mysql/conf.d/
if [ ! -f ~/.my.cnf ] ; then
  cp -v /vagrant/config/mysql/vagrant-my.cnf ~vagrant/.my.cnf
  chown vagrant: ~vagrant/.my.cnf
  chmod 600 ~vagrant/.my.cnf
fi
service mysql restart
mysql -e '
  CREATE DATABASE IF NOT EXISTS vagrant;
  GRANT ALL ON vagrant.* TO vagrant@localhost IDENTIFIED BY "vagrant";
  GRANT SUPER ON *.* TO vagrant@localhost;
'
