#!/bin/bash
echo "Installing CiviCRM tools to Vagrant environment."
curl -LsS https://download.civicrm.org/cv/cv.phar -o /usr/local/bin/cv
chmod +x /usr/local/bin/cv
