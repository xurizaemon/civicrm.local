#!/bin/bash
echo "Configuring OS packages for Vagrant environment."
which python || apt update && apt -y install python python-apt etckeeper
apt update && apt -y upgrade
apt -y install curl mariadb-server nginx-light php7.2-fpm php7.2-cli php7.2-imap php7.2-ldap php7.2-curl php7.2-mysql php7.2-intl php7.2-gd php7.2-dev php7.2-bcmath php7.2-mbstring php7.2-soap php7.2-zip php7.2-xml
phpenmod bcmath curl imap xml
